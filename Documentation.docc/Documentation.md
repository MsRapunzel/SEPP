# ``CryptoiOS``

Your All-in-One Crypto Companion!

## Overview

CryptoiOS is a cryptocurrency app that downloads live price data from an API and saves the current user's portfolio.

## Topics

### Utitilies

- ``NetworkingManager``
- ``LocalFileManager``

### Extensions

- ``ColorTheme``
- ``DeveloperPreview``

### Services

- ``CoinDataService``
- ``CoinDetailDataService``
- ``CoinImageService``
- ``MarketDataService``

### Models

- ``CoinModel``
- ``CoinDetailModel``
- ``StatisticModel``
- ``MarketDataModel``

### CoinImage
- ``CoinImageView``
- ``CoinImageViewModel``

### CircleButton
- ``CircleButtonView``
- ``CircleButtonAnimationView``

### Components
- ``SearchBarView``
- ``StatisticView``

### Home – ViewModels
- ``HomeViewModel``

### Home – Views
- ``HomeView``
- ``HomeStatsView``
- ``CoinRowView``

### Detail – ViewModels
- ``DetailViewModel``

### Detail – Views
- ``DetailView``
- ``ChartView``
